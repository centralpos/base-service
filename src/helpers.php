<?php

use Illuminate\Auth\GenericUser;

if (! function_exists('user')) {
    /**
     * Get the configuration path.
     *
     * @param  string|null $attribute
     * @return GenericUser|mixed
     */
    function user($attribute = null)
    {
        $user = app('auth')->user();

        return $attribute ? $user->{$attribute} : $user;
    }
}

if (! function_exists('user_from_token')) {
    /**
     * Get the configuration path.
     *
     * @param  string $token
     * @return GenericUser
     */
    function user_from_token($token)
    {
        list(,$payload,) = explode('.', $token);

        $userAttributes = json_decode(base64_decode($payload), true);

        return new GenericUser($userAttributes);
    }
}

if (! function_exists('zip_file')) {
    /**
     * @param  string $filepath
     * @param  string $zipPath
     * @param  bool $unlink
     * @return string
     * @throws Exception
     */
    function zip_file($filepath, $zipPath = '', $unlink = true)
    {
        $zip = new ZipArchive();
        $pathinfo = pathinfo($filepath);

        if (!$zipPath) {
            $zipPath = $pathinfo['dirname']."/".$pathinfo['filename'].".zip";
        }

        if ($zip->open($zipPath, ZipArchive::CREATE) !== TRUE) {
            throw new Exception('No se pudo crear el archivo '. $zipPath);
        }

        $zip->addFile($filepath, $pathinfo['basename']);
        $zip->close();

        if ($unlink) {
            unlink($filepath);
        }

        return $zipPath;
    }
}

if (! function_exists('cache')) {
    /**
     * Get / set the specified cache value.
     *
     * If an array is passed, we'll assume you want to put to the cache.
     *
     * @param  dynamic  key|key,default|data,expiration|null
     * @return mixed|\Illuminate\Cache\CacheManager
     *
     * @throws \Exception
     */
    function cache()
    {
        $arguments = func_get_args();

        if (empty($arguments)) {
            return app('cache');
        }

        if (is_string($arguments[0])) {
            return app('cache')->get(...$arguments);
        }

        if (! is_array($arguments[0])) {
            throw new Exception(
                'When setting a value in the cache, you must pass an array of key / value pairs.'
            );
        }

        return app('cache')->put(key($arguments[0]), reset($arguments[0]), $arguments[1] ?? null);
    }
}
