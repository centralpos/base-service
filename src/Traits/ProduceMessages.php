<?php

namespace Centralpos\BaseService\Traits;

use Exception;
use RdKafka\Conf;
use RdKafka\Producer;

trait ProduceMessages
{
    /**
     * RdKafka producer
     *
     * @var Producer
     */
    protected $producer;

    /**
     * Get topic
     *
     * @return string
     * @throws Exception
     */
    abstract protected function topic();

    /**
     * Produce and send a single message to broker
     *
     * @param  array|string $message
     * @param  mixed $key
     * @param  array $headers
     * @return int|bool
     * @throws Exception
     */
    public function send($message, $key = null, array $headers = [])
    {
        if ($this->sendToLog()) {
            return $this->logMessage($message, $key, $headers);
        }

        $payload = $this->buildPayload($message, $headers);
        $topic = $this->producer()->newTopic($this->topic());

        // RD_KAFKA_PARTITION_UA, lets librdkafka choose the partition.
        // Messages with the same "$key" will be in the same topic partition.
        // This ensure that messages are consumed in order.
        $topic->produce(RD_KAFKA_PARTITION_UA, 0, $payload, $key);

        if ($this->pollEnabled()) {
            $this->poll();
        }

        if ($this->flushEnabled()) {
            return $this->flush();
        }

        return true;
    }

    /**
     * @param  string $message
     * @param  string|int $key
     * @param  array $headers
     */
    protected function logMessage($message, $key, $headers)
    {
        $topic = $this->topic();
        $payload = $this->buildPayload($message, $headers);

        info(compact('topic', 'key', 'payload'));
    }

    /**
     * librdkafka flush waits for all outstanding producer requests to be handled.
     * It ensures messages produced properly.
     *
     * @param  int $timeout "timeout in milliseconds"
     * @return int
     */
    protected function flush(int $timeout = 10000)
    {
        return $this->producer()->flush($timeout);
    }

    /**
     * @return bool
     */
    protected function flushEnabled()
    {
        return env('KAFKA_FLUSH_ENABLED', true);
    }

    /**
     *
     */
    protected function poll()
    {
        return $this->producer()->poll(0);
    }

    /**
     * @return bool
     */
    protected function pollEnabled()
    {
        return env('KAFKA_POLL_ENABLED', false);
    }

    /**
     * Build kafka message payload
     *
     * @param  array|string $message
     * @param  array $headers
     * @return string
     */
    protected function buildPayload($message, array $headers = [])
    {
        return json_encode([
            'body' => $message,
            'headers' => $headers
        ]);
    }

    /**
     * @return Producer
     */
    protected function producer()
    {
        if (empty($this->producer)) {

            $conf = new Conf();

            $conf->set('metadata.broker.list', $this->brokerList());
            $conf->set('compression.type', 'snappy');

            if (env('KAFKA_DEBUG', false)) {
                $conf->set('log_level', LOG_DEBUG);
                $conf->set('debug', 'all');
            }

            $this->producer = new Producer($conf);
        }

        return $this->producer;
    }

    /**
     * @return string|null
     */
    protected function brokerList()
    {
        return env('KAFKA_BROKERS', 'log');
    }

    /**
     * @return bool
     */
    protected function sendToLog()
    {
        return $this->brokerList() == 'log';
    }
}
