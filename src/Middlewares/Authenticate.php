<?php

namespace Centralpos\BaseService\Middlewares;

use Closure;
use Illuminate\Http\Response;

class Authenticate
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validSecrets = explode(',', env('AUTH_SECRETS'));
        $authorization = $request->header('apikey');

        if ($authorization && in_array($authorization, $validSecrets)) {
            return $next($request);
        }

        abort(Response::HTTP_UNAUTHORIZED);
    }
}
