<?php


namespace Centralpos\BaseService\Parsers;

use Exception;
use Illuminate\Support\Str;

abstract class FixedLengthStringParser
{
    /**
     * @param  string $line
     * @param  string $type
     * @param  string $lineEnding
     * @return array
     * @throws Exception
     */
    public static function toArray(string $line, $type = '', $lineEnding = "\r\n")
    {
        return (new static())->parseString($line, $type, $lineEnding);
    }

    /**
     * @param  array $row
     * @param  string $type
     * @return string
     * @throws Exception
     */
    public static function toStr(array $row, $type = '')
    {
        return (new static())->parseArray($row, $type);
    }

    /**
     * @param  array $row
     * @param  string $type
     * @return string
     * @throws Exception
     */
    public function parseArray(array $row, $type = '')
    {
        $type = $this->resolveType($row, $type);
        $row = $this->addDefaultValues($row, $type);
        $this->validateItems($row, $type);

        $line = '';
        $config = $this->getConfig($type);

        foreach ($this->fieldsName($type) as $fieldName) {
            $value = $row[$fieldName];
            $this->validator($value, $fieldName);
            $str = $this->formatter($value, $fieldName);

            if (strlen($str) !== $this->configLength($config, $fieldName)) {
                throw new Exception("Invalid value length for field [$fieldName]");
            }

            $line .= $str;
        }

        $this->validateLength($line, $type);

        return $line;
    }

    /**
     * @param  string $line
     * @param  string $type
     * @param  string $lineEnding
     * @return array
     * @throws Exception
     */
    public function parseString(string $line, $type = '', $lineEnding = "\r\n")
    {
        $type = $this->resolveType($line, $type);
        $line = $this->removeLineEnding($line, $lineEnding);
        $this->validateLength($line, $type);

        $row = [];
        $currentPos = 0;
        $config = $this->getConfig($type);

        foreach ($this->fieldsName($type) as $fieldName) {
            $length = $this->configLength($config, $fieldName);
            $value = substr($line, $currentPos, $length);

            $this->validator($value, $fieldName, 'str');

            $row[$fieldName] = $this->formatter($value, $fieldName, 'str');
            $currentPos += $length;
        }

        $this->validateItems($row, $type);

        return $row;
    }

    /**
     * @param  array $row
     * @param  string $type
     * @return array
     * @throws Exception
     */
    protected function addDefaultValues(array $row, string $type)
    {
        $config = $this->getConfig($type);

        foreach ($config as $fieldName => $value) {
            if (is_int($value)) {
                unset($config[$fieldName]);
            }
        }

        $row = array_merge($config, $row);

        return $row;
    }

    /**
     * @param  string $line
     * @param  string $type
     * @return bool
     * @throws Exception
     */
    protected function validateLength(string $line, string $type)
    {
        $expected = $this->configLength($this->getConfig($type));
        $received = strlen($line);

        if ($received != $expected) {
            throw new Exception("Invalid [$type] line length. Expected: $expected Received: $received");
        }

        return true;
    }

    /**
     * @param  array $config
     * @param  null|string $fieldName
     * @return int
     */
    protected function configLength(array $config, $fieldName = null)
    {
        if ($fieldName) {
            $value = $config[$fieldName];
            return is_int($value) ? $value : strlen($value);
        }

        $length = 0;

        foreach ($config as $name => $value) {
            $length += is_int($value) ? $value : strlen($value);
        }

        return $length;
    }

    /**
     * @param  array $row
     * @param  string $type
     * @return bool
     * @throws Exception
     */
    protected function validateItems(array $row, string $type)
    {
        $expected = count($this->getConfig($type));
        $received = count($row);

        if ($received != $expected) {
            throw new Exception("Invalid [$type] number of items. Expected: $expected Received: $received");
        }

        return true;
    }

    /**
     * @param  string $type
     * @return array
     * @throws Exception
     */
    protected function fieldsName(string $type)
    {
        return array_keys($this->getConfig($type));
    }

    /**
     * @param  string $line
     * @param  string $lineEnding
     * @return string
     */
    protected function removeLineEnding(string $line, string $lineEnding)
    {
        return preg_replace("/{$lineEnding}/", "", $line);
    }

    /**
     * @param  mixed $value
     * @param  string $fieldName
     * @param  string $type
     * @return mixed
     */
    protected function formatter($value, string $fieldName, $type = '')
    {
        $type .= " formatter";
        $method = $this->methodSearch($type, $fieldName);

        if ($method) {
            return $this->{$method}($value);
        }

        return $value;
    }

    /**
     * @param  string $value
     * @param  string $fieldName
     * @param  string $type
     * @return true
     * @throws Exception
     */
    protected function validator(string $value, string $fieldName, $type = '')
    {
        $type .= " validator";
        $method = $this->methodSearch($type, $fieldName);

        if ($method && $this->{$method}($value) === false) {
            throw new Exception("$type for $fieldName failed [$value]");
        }

        return true;
    }

    /**
     * @param  string $type
     * @param  string $name
     * @return false|string
     */
    protected function methodSearch(string $type, string $name)
    {
        $methodName = Str::camel("$name $type");

        if (!method_exists($this, $methodName)) {
            return false;
        }

        return $methodName;
    }

    /**
     * @param  string $type
     * @return array
     * @throws Exception
     */
    protected function getConfig(string $type)
    {
        $methodName = $this->methodSearch('config', $type);

        if (!$methodName) {
            throw new Exception("Configuration for type [$type] not found");
        }

        return $this->{$methodName}();
    }

    /**
     * @param  string $type
     * @param  string $expected
     * @return void
     * @throws Exception
     */
    public function configIsValid(string $type, string $expected)
    {
        $configured = $this->configLength($this->getConfig($type));
        $success = $configured === $expected;

        dd(compact('type', 'success', 'configured', 'expected'));
    }

    /**
     * @param  string|array $value
     * @param  string $type
     * @return string
     * @throws Exception
     */
    protected function resolveType($value, string $type)
    {
        if ($type) {
            return $type;
        }

        if (method_exists($this, 'getRowType')) {
            return $this->getRowType($value);
        }

        throw new Exception("undefined type");
    }
}
