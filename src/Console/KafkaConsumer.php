<?php


namespace Centralpos\BaseService\Console;


use Exception;
use Illuminate\Console\Command;
use RdKafka\Conf;
use RdKafka\KafkaConsumer as Consumer;
use RdKafka\Message;

class KafkaConsumer extends Command
{
    /**
     * @var string
     */
    protected $signature = 'kafka:consume {topics} {job}';

    /**
     * @throws \RdKafka\Exception
     */
    public function handle()
    {
        $consumer = new Consumer($this->getConfig());

        $consumer->subscribe($this->topics());

        while (true) {
            $message = $consumer->consume(120*1000);
            switch ($message->err) {
                case RD_KAFKA_RESP_ERR_NO_ERROR:
                    $this->processMessage($message);
                    $consumer->commitAsync($message);
                    break;
                case RD_KAFKA_RESP_ERR__PARTITION_EOF:
                    echo "No more messages; will wait for more\n";
                    break;
                case RD_KAFKA_RESP_ERR__TIMED_OUT:
                    echo "Timed out\n";
                    break;
                default:
                    throw new Exception($message->errstr(), $message->err);
            }
        }
    }

    protected function processMessage(Message $message)
    {
        $payload = $this->decodeMessage($message);
        $jobClass = $this->argument('job');

        $job = new $jobClass($payload);

        dispatch($job);
    }

    /**
     *
     * @param  Message $kafkaMessage
     * @return array
     */
    protected function decodeMessage(Message $kafkaMessage)
    {
        $message = json_decode($kafkaMessage->payload, true);

        if (is_string($message['body'])) {
            $message['body'] = json_decode($message['body'], true);
        }

        return $message;
    }

    /**
     * @returns array
     */
    protected function topics()
    {
        return explode(',', $this->argument('topics'));
    }

    /**
     * Get kafka config
     *
     * @return Conf
     */
    protected function getConfig()
    {
        $conf = new Conf();

        $conf->set('group.id', config('app.name'));
        $conf->set('metadata.broker.list', env('KAFKA_BROKERS', 'kafka:9092'));
        $conf->set('auto.offset.reset', 'smallest');
        $conf->set('enable.auto.commit', 'false');

        return $conf;
    }
}
