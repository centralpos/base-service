<?php

namespace Centralpos\BaseService;

use Firebase\JWT\JWT;
use GuzzleHttp\Client;
use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Support\ServiceProvider;

class BaseServiceProvider extends ServiceProvider
{

    const KEYCLOAK_CACHE_KEY = 'keycloak_public_key';

    public function boot()
    {
        $this->userAuth();
        $this->defineGate();
        $this->addLogstashChannel();
        $this->setDefaultQueueName();
    }

    protected function defineGate()
    {
        $this->app[Gate::class]->before(function ($user, $ability) {
            return in_array($ability, $user->roles);
        });
    }

    protected function setDefaultQueueName()
    {
        $this->app->configure('queue');

        $this->app['config']->set(
            'queue.connections.redis.queue',
            env('APP_NAME', 'default')
        );
    }

    protected function addLogstashChannel()
    {
        $this->app->configure('logging');

        $loggingChannels = array_merge(
            $this->app['config']->get('logging.channels', []),
            require realpath(__DIR__ . '/../config/logging.php')
        );

        $this->app['config']->set('logging.channels', $loggingChannels);
    }

    protected function userAuth()
    {
        $this->app['auth']->viaRequest('api', function ($request) {

            try {
                $decodedToken = JWT::decode(
                    $request->bearerToken(),
                    $this->buildKeycloakPublicKey(),
                    ['RS256']
                );

                return new GenericUser((array) $decodedToken);
            } catch (\Exception $exception) {
                return null;
            }
        });
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    protected function buildKeycloakPublicKey()
    {
        $publicKey = wordwrap($this->getKeycloakPublicKey(), 64, "\n", true);

        return "-----BEGIN PUBLIC KEY-----\n"
            . $publicKey
            . "\n-----END PUBLIC KEY-----";
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    protected function getKeycloakPublicKey()
    {
        if (!cache()->has(self::KEYCLOAK_CACHE_KEY)) {

            $client = new Client();
            $response = $client->get(env('KEYCLOAK_REALM_URL'));

            $config = json_decode($response->getBody(), true);

            cache()->put(
                self::KEYCLOAK_CACHE_KEY,
                $config['public_key'],
                env('KEYCLOAK_CACHE_TTL', 3600)
            );
        }

        return cache(self::KEYCLOAK_CACHE_KEY);
    }
}
