<?php

use Centralpos\BaseService\Logging\LogstashLogger;

return [

    'stack' => [
        'driver' => 'stack',
        'channels' => ['daily', 'logstash'],
    ],

    'logstash' => [
        'driver' => 'custom',
        'via'    => LogstashLogger::class,
        'host'   => env('LOGSTASH_HOST', 'logstash'),
        'port'   => env('LOGSTASH_PORT', 5000),
    ],
];